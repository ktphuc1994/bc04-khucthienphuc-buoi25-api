function SinhVien(id, maSV, name, email, password, math, physics, chemistry) {
  this.id = id;
  this.maSV = maSV;
  this.name = name;
  this.email = email;
  this.password = password;
  this.math = math;
  this.physics = physics;
  this.chemistry = chemistry;
  this.diemTB = function () {
    return (
      Math.floor(((math * 1 + physics * 1 + chemistry * 1) * 100) / 3) / 100
    );
  };
}
