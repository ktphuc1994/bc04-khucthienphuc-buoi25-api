const tbodySV = document.getElementById("tbodySinhVien");

// START LOADING SCREEN
function turnOnLoading() {
  document.getElementById("loading").style.display = "flex";
}
function turnOffLoading() {
  document.getElementById("loading").style.display = "none";
}
// END LOADING SCREEN

// START LẤY THÔNG TIN TRÊN FORM
function layThongTinForm() {
  var maSV = document.getElementById("txtMaSV").value;
  var name = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var password = document.getElementById("txtPass").value;
  var math = document.getElementById("txtDiemToan").value;
  var physics = document.getElementById("txtDiemLy").value;
  var chemistry = document.getElementById("txtDiemHoa").value;
  return new SinhVien(0, maSV, name, email, password, math, physics, chemistry);
}
// END LẤY THÔNG TIN TRÊN FORM

// START XÓA THÔNG TIN TRÊN FORM
function xoaThongTinForm() {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtMaSV").disabled = false;
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
}
// END XÓA THÔNG TIN TRÊN FORM

// START XÓA THÔNG BÁO LỖI TRÊN FORM
function xoaThongBaoLoiTrenFrom() {
  document.getElementById("spanMaSV").innerText = "";
  document.getElementById("spanTenSV").innerText = "";
  document.getElementById("spanEmailSV").innerText = "";
  document.getElementById("spanMatKhau").innerText = "";
  document.getElementById("spanToan").innerText = "";
  document.getElementById("spanLy").innerText = "";
  document.getElementById("spanHoa").innerText = "";
}
// END XÓA THÔNG BÁO LỖI TRÊN FORM

// START SHOW THÔNG TIN LÊN FORM
function showThongTinForm(sv) {
  document.getElementById("txtMaSV").value = sv.maSV;
  document.getElementById("txtMaSV").disabled = true;
  document.getElementById("txtTenSV").value = sv.name;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.password;
  document.getElementById("txtDiemToan").value = sv.math;
  document.getElementById("txtDiemLy").value = sv.physics;
  document.getElementById("txtDiemHoa").value = sv.chemistry;
}
// END SHOW THÔNG TIN LÊN FORM

// START TẠO TR CHO MỖI SINH VIÊN ĐỂ RENDER RA MÀN HÌNH
function createSvTR(sv) {
  var newTR = document.createElement("tr");
  var contentTR = `<td class="align-middle text-center" data-id="${sv.id}">${
    sv.maSV
  }</td>
    <td class="align-middle">${sv.name}</td>
    <td class="align-middle">${sv.email}</td>
    <td class="align-middle text-center">${sv.diemTB()}</td>
    <td><button class="btn btn-warning" onclick="suaSV('${sv.id}')">Sửa</button>
    <button class="btn btn-danger" onclick="xoaSV('${sv.id}')">Xóa</button>
    </td>`;
  newTR.innerHTML = contentTR;
  return newTR;
}
// END TẠO TR CHO MỖI SINH VIÊN ĐỂ RENDER RA MÀN HÌNH

// START RENDER DSSV RA MÀN HÌNH
function renderDSSV(dssv, whereToRender) {
  document.getElementById(whereToRender).innerHTML = "";
  var renderAddress = document.getElementById(whereToRender);
  dssv.forEach(function (sv) {
    renderAddress.append(createSvTR(sv));
  });
}
// START RENDER DSSV RA MÀN HÌNH

// START LẤY INDEX CỦA TR TƯƠNG ỨNG VỚI VALUE
function getIndexOfTR(value, indexOfTd) {
  var svTrList = document.querySelectorAll("#tbodySinhVien tr");
  return Array.from(svTrList).findIndex(function (svTR) {
    return svTR.children[indexOfTd].dataset.id == value;
  });
}
// START LẤY INDEX CỦA TR TƯƠNG ỨNG VỚI VALUE

// START KIỂM TRA ID SINH VIÊN
function checkId(sv, dssv) {
  var isValid =
    validation.kiemTraRong(sv.maSV, "spanMaSV", "Mã SV") &&
    validation.kiemTraDoDai(
      sv.maSV,
      "spanMaSV",
      "Mã sinh viên phải là số, từ 1 đến 6 ký số",
      1,
      6
    ) &&
    validation.kiemTraSo(sv.maSV, "spanMaSV", "Mã SV") &&
    validation.checkDuplicate(
      sv.maSV,
      dssv,
      "maSV",
      "spanMaSV",
      "Mã Sinh Viên này đã được sử dụng, vui lòng chọn mã khác"
    );
  return isValid;
}
// END KIỂM TRA ID SINH VIÊN

// START KIÊM TRA THÔNG TIN INPUT TRÊN FORM (TRỪ TRƯỜNG ID)
function checkFormValidationWithoutId(sv) {
  var isValid =
    validation.kiemTraRong(sv.name, "spanTenSV", "Tên SV") &&
    validation.kiemTraTen(
      sv.name,
      "spanTenSV",
      "Tên không bao gồm số và ký tự đặc biệt"
    );

  isValid &=
    validation.kiemTraRong(sv.email, "spanEmailSV", "Email") &&
    validation.kiemTraEmail(sv.email, "spanEmailSV", "Email sai định dạng");

  isValid &=
    validation.kiemTraRong(sv.password, "spanMatKhau", "Mật khẩu") &&
    validation.checkPassword(
      sv.password,
      "spanMatKhau",
      "Mật khẩu phải bao gồm ít nhất 1 ký tự viết Hoa, 1 ký tự thường, 1 ký số và 1 ký tự đặc biệt (!@#$%^&*?)"
    );

  isValid &=
    validation.kiemTraRong(sv.math, "spanToan", "Điểm Toán") &&
    validation.kiemTraSo(sv.math, "spanToan", "Điểm Toán") &&
    validation.kiemTraGiaTri(sv.math * 1, "spanToan", "Điểm Toán", 0, 10);

  isValid &=
    validation.kiemTraRong(sv.physics, "spanLy", "Điểm Lý") &&
    validation.kiemTraSo(sv.physics, "spanLy", "Điểm Lý") &&
    validation.kiemTraGiaTri(sv.physics * 1, "spanLy", "Điểm Lý", 0, 10);

  isValid &=
    validation.kiemTraRong(sv.chemistry, "spanHoa", "Điểm Hóa") &&
    validation.kiemTraSo(sv.chemistry, "spanHoa", "Điểm Hóa") &&
    validation.kiemTraGiaTri(sv.chemistry * 1, "spanHoa", "Điểm Hóa", 0, 10);
  return isValid;
}
// END KIÊM TRA THÔNG TIN INPUT TRÊN FORM (TRỪ TRƯỜNG ID)
