const BASE_URL = "https://62db6ca0d1d97b9e0c4f3326.mockapi.io";
const BUOI25 = "buoi25_SV";
const btnThemSV = document.getElementById("btnThemSV");
const btnCapNhatSV = document.getElementById("btnCapNhatSV");

btnCapNhatSV.style.display = "none";

// START LẤY DSSV TRÊN API RỒI ĐƯA LÊN MÀN HÌNH
function getDSSV() {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/${BUOI25}`,
    method: "GET",
  })
    .then(function (res) {
      // console.log(res);
      var tempDSSV = res.data.map((sv) => {
        return new SinhVien(
          sv.id,
          sv.maSV,
          sv.name,
          sv.email,
          sv.password,
          sv.math,
          sv.physics,
          sv.chemistry
        );
      });
      renderDSSV(tempDSSV, "tbodySinhVien");
      turnOffLoading();
    })
    .catch(function (err) {
      console.log(err);
      turnOffLoading();
    });
}
getDSSV();
// END LẤY DSSV TRÊN API RỒI ĐƯA LÊN MÀN HÌNH

// START THÊM SINH VIÊN
function themSV() {
  var newSV = layThongTinForm();
  turnOnLoading();
  axios({
    url: `${BASE_URL}/${BUOI25}`,
    method: "GET",
  })
    .then(function (res) {
      // console.log(res);
      var isValid =
        checkId(newSV, res.data) & checkFormValidationWithoutId(newSV);
      if (isValid) {
        axios({
          url: `${BASE_URL}/${BUOI25}`,
          method: "POST",
          data: newSV,
        })
          .then(function (res2) {
            // console.log(res2);
            tbodySV.append(createSvTR(res2.data));
            xoaThongTinForm();
            turnOffLoading();
          })
          .catch(function (err2) {
            console.log(err2);
            turnOffLoading();
          });
      } else {
        turnOffLoading();
      }
    })
    .catch(function (err) {
      console.log(err);
      turnOffLoading();
    });
}
// END THÊM SINH VIÊN

// START SỬA SINH VIÊN
function suaSV(svID) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/${BUOI25}/${svID}`,
    method: "GET",
  })
    .then(function (res) {
      // console.log(res);
      showThongTinForm(res.data);
      btnThemSV.style.display = "none";
      btnCapNhatSV.style.display = "inline-block";
      btnCapNhatSV.dataset.id = svID;
      turnOffLoading();
    })
    .catch(function (err) {
      console.log(err);
      turnOffLoading();
    });
}
// END SỬA SINH VIÊN

// START CẬP NHẬT SINH VIÊN
function capNhatThongTinForm() {
  var updatedSV = layThongTinForm();
  var isValid = checkFormValidationWithoutId(updatedSV);
  if (isValid) {
    turnOnLoading();
    var updatedSvID = btnCapNhatSV.dataset.id;
    axios({
      url: `${BASE_URL}/${BUOI25}/${updatedSvID}`,
      method: "PUT",
      data: updatedSV,
    })
      .then(function (res) {
        // console.log(res);
        tempUpdatedSV = new SinhVien(
          res.data.id,
          res.data.maSV,
          res.data.name,
          res.data.email,
          res.data.password,
          res.data.math,
          res.data.physics,
          res.data.chemistry
        );
        tbodySV.children[getIndexOfTR(tempUpdatedSV.id, 0)].replaceWith(
          createSvTR(tempUpdatedSV)
        );
        xoaThongTinForm();
        btnThemSV.style.display = "inline-block";
        btnCapNhatSV.style.display = "none";
        delete btnCapNhatSV.dataset.id;
        turnOffLoading();
      })
      .catch(function (err) {
        console.log(err);
        turnOffLoading();
      });
  }
}
// END CẬP NHẬT SINH VIÊN

// START XÓA SINH VIÊN
function xoaSV(svID) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/${BUOI25}/${svID}`,
    method: "DELETE",
  })
    .then(function (res) {
      // console.log(res);
      tbodySV.children[getIndexOfTR(res.data.id, 0)].remove();
      turnOffLoading();
    })
    .catch(function (err) {
      console.log(err);
      turnOffLoading();
    });
}
// END XÓA SINH VIÊN

// START RESET FORM
function resetDSSV() {
  xoaThongTinForm();
  xoaThongBaoLoiTrenFrom();
  document.getElementById("txtMaSV").disabled = false;
  btnThemSV.style.display = "inline-block";
  btnCapNhatSV.style.display = "none";
  delete btnCapNhatSV.dataset.id;
}
// END RESET FORM

// START Enter to Search
document
  .getElementById("txtSearch")
  .addEventListener("keypress", function (event) {
    if (event.key === "Enter") {
      event.preventDefault;
      document.getElementById("btnSearch").click();
    }
  });
// END Enter to Search

// START Tìm kiếm Sinh Viên
document.getElementById("btnSearch").onclick = function () {
  turnOnLoading();
  var tenSinhVien = document.getElementById("txtSearch").value;
  axios({
    url: `${BASE_URL}/${BUOI25}`,
    method: "GET",
  })
    .then(function (res) {
      // console.log(res);
      var dssvTheoTen = res.data.filter(function (sv) {
        return sv.name.search(tenSinhVien) != -1;
      });
      dssvTheoTen = dssvTheoTen.map((sv) => {
        return new SinhVien(
          sv.id,
          sv.maSV,
          sv.name,
          sv.email,
          sv.password,
          sv.math,
          sv.physics,
          sv.chemistry
        );
      });
      renderDSSV(dssvTheoTen, "tbodySinhVien");
      turnOffLoading();
    })
    .catch(function (err) {
      console.log(err);
      turnOffLoading();
    });
};
// END Tìm kiếm Sinh Viên
